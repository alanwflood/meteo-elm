const router = require("koa-router")();
const fetch = require("node-fetch");
const cors = require("@koa/cors");
const Koa = require("koa");

const app = (module.exports = new Koa());
app.use(cors());

router.get("/", ctx => {
  ctx.body = {
    status: "success",
    message: "hello, world!"
  };
});

router.get("/weather/:coordinates", async (ctx, next) => {
  const data = await fetch(
    `https://api.darksky.net/forecast/fb1d010cc38cef552367b668f0e09e4b/${
      ctx.params.coordinates
    }`
  )
    .then(res => res.json())
    .catch(err => err);
  return (ctx.body = await data);
});

app.use(router.routes());
if (!module.parent) app.listen(5000);
