port module Main exposing (main)

import Browser exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onSubmit)
import Http
import Json.Decode as Decode exposing (Decoder, Value, float, int, list, map, null, nullable, string, succeed)
import Json.Decode.Pipeline exposing (optional, optionalAt, required, requiredAt)
import RemoteData exposing (RemoteData(..), WebData)
import Time



--Main


main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- Model


init : Value -> ( Model, Cmd Msg )
init flags =
    let
        coordinates =
            Coordinates 0 0
    in
    ( { weather = RemoteData.NotAsked, coordinates = coordinates }, initSearch coordinates )


type alias Model =
    { weather : WebData Weather
    , coordinates : Coordinates
    }


type alias Coordinates =
    { lat : Float
    , lng : Float
    }


type alias Weather =
    { latitude : Float
    , longitude : Float
    , timezone : String
    , currently : Maybe CurrentWeather
    , dailyWeather : Maybe (List DailyWeather)
    }


type alias CurrentWeather =
    { summary : String
    , icon : String
    , nearestStormDistance : Maybe Int
    , nearestStormBearing : Maybe Int
    , temperature : Float
    , apparentTemperature : Float
    , apparentTemperatureMax : Float
    , apparentTemperatureMin : Float
    }


type alias DailyWeather =
    { time : Time.Posix
    , sunrise : Time.Posix
    , sunset : Time.Posix
    , precipitationChance : Float
    , windSpeed : Float
    , temperatureMax : Float
    , temperatureMin : Float
    }



--Json


decodeWeather : Decoder Weather
decodeWeather =
    succeed Weather
        |> required "latitude" float
        |> required "longitude" float
        |> required "timezone" string
        |> optional "currently" (map Just currentWeatherDecoder) Nothing
        |> optionalAt [ "daily", "data" ] (map Just (list dailyWeatherDecoder)) Nothing


currentWeatherDecoder : Decoder CurrentWeather
currentWeatherDecoder =
    succeed CurrentWeather
        |> required "summary" string
        |> required "icon" string
        |> optional "nearestStormDistance" (map Just int) Nothing
        |> optional "nearestStormBearing" (map Just int) Nothing
        |> required "temperature" float
        |> optional "apparentTemperature" float 0
        |> optional "apparentTemperatureMax" float 0
        |> optional "apparentTemperatureMin" float 0


dailyWeatherDecoder : Decoder DailyWeather
dailyWeatherDecoder =
    succeed DailyWeather
        |> required "time" (int |> map Time.millisToPosix)
        |> required "sunriseTime" (int |> map Time.millisToPosix)
        |> required "sunsetTime" (int |> map Time.millisToPosix)
        |> required "precipProbability" float
        |> required "windSpeed" float
        |> required "temperatureMax" float
        |> required "temperatureMin" float



-- Update


type Msg
    = RecieveWeather (Result Http.Error Weather)
    | FetchWeather Coordinates
    | UpdateCoordinates Coordinates


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        RecieveWeather (Ok json) ->
            ( { model | weather = Success json }, Cmd.none )

        RecieveWeather (Err e) ->
            ( { model | weather = Failure e }, Cmd.none )

        FetchWeather coordinates ->
            ( { model | weather = Loading }, getLocation coordinates )

        UpdateCoordinates coordinates ->
            ( { model | coordinates = coordinates, weather = Loading }, getLocation coordinates )



-- Http Requests


getLocation : Coordinates -> Cmd Msg
getLocation { lat, lng } =
    let
        coordinates =
            String.fromFloat lat ++ "," ++ String.fromFloat lng

        url =
            "http://localhost:5000/weather/" ++ coordinates
    in
    Http.get
        { url = url
        , expect = Http.expectJson RecieveWeather decodeWeather
        }



-- Subscriptions / Ports


port initSearch : Coordinates -> Cmd msg


port setCoordinates : (Coordinates -> msg) -> Sub msg


subscriptions : Model -> Sub Msg
subscriptions model =
    setCoordinates UpdateCoordinates



-- View


showWeather : WebData Weather -> Html Msg
showWeather weather =
    case weather of
        NotAsked ->
            text ""

        Loading ->
            text "Loading"

        Failure e ->
            errorHtml e

        Success weatherData ->
            div [] [ div [] [ weatherTable weatherData ] ]


errorHtml : Http.Error -> Html Msg
errorHtml error =
    let
        errorMsg =
            case error of
                Http.BadUrl url ->
                    "I was expecting a valid URL, but I got the url: " ++ url

                Http.Timeout ->
                    "It took too long to get a response from the server!"

                Http.NetworkError ->
                    "Unable to make a connection. Is your network working?"

                Http.BadStatus response ->
                    "I failed due to a bad status: " ++ String.fromInt response

                Http.BadBody errorMessage ->
                    "I failed because of the following error: "
                        ++ errorMessage
    in
    pre [] [ text errorMsg ]


weatherTableRow : String -> String -> Html Msg
weatherTableRow attribute value =
    tr [] [ td [] [ text attribute ], td [] [ text value ] ]


weatherTable : Weather -> Html Msg
weatherTable { latitude, longitude, timezone, currently } =
    let
        latLong =
            String.fromFloat latitude ++ ", " ++ String.fromFloat longitude
    in
    table []
        [ tbody []
            ([ weatherTableRow "Lat/Lon" latLong
             , weatherTableRow "Timezone" timezone
             ]
                ++ currentWeatherTable currently
            )
        ]


currentWeatherTable : Maybe CurrentWeather -> List (Html Msg)
currentWeatherTable currentWeather =
    case currentWeather of
        Just { temperature, nearestStormDistance, nearestStormBearing } ->
            [ Maybe.withDefault (text "")
                (Maybe.map
                    (\n -> weatherTableRow "Nearest Storm Bearing" <| String.fromInt n ++ String.fromChar (Char.fromCode 176))
                    nearestStormBearing
                )
            , Maybe.withDefault (text "")
                (Maybe.map
                    (\n -> weatherTableRow "Nearest Storm Distance" <| String.fromInt n ++ " miles ")
                    nearestStormDistance
                )
            ]

        Nothing ->
            []


weatherView : WebData Weather -> Html Msg
weatherView weather =
    div [ class "weather-container" ] [ showWeather weather ]


locationInput : Html Msg
locationInput =
    div [ class "w-full max-w-md mx-auto pt-10 pb-10 px-5" ]
        [ div [ class "flex items-center border-b border-b-2 border-teal py-2" ]
            [ label [ for "placesAutocomplete", class "flex-2 pr-10" ] [ text "I want to know the weather in: " ]
            , input [ id "placesAutocomplete", class "flex-1 appearance-none bg-transparent border-none w-auto text-grey-darker mr-3 py-1 px-2 leading-tight focus:outline-none", placeholder "Enter a City/Country/Location" ] []
            ]
        ]


banner : Html Msg
banner =
    div [ class "bg-red text-center text-white py-8" ]
        [ h1 [ class "font-sans pb-5" ] [ text "Weather Me This" ]
        , p [ class "font-serif" ] [ text "Your 5 day weather forecast at a glance" ]
        ]


view : Model -> Html Msg
view model =
    div []
        [ banner
        , locationInput
        , weatherView model.weather
        ]
