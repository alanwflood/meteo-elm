// Styles
import("./assets/stylesheets/tailwind.css");

// Elm
import { Elm } from "./elm/Main.elm";
const node = document.createElement("div");
document.body.appendChild(node);
var app = Elm.Main.init({ node });
console.log(app);

function initSearch(callback) {
  // Geocode + Establishment
  const searchBox = new google.maps.places.Autocomplete(
    document.getElementById("placesAutocomplete")
  );

  searchBox.addListener("place_changed", function() {
    const location = searchBox.getPlace();
    // Just returning name for testing
    const result = {
      lat: location.geometry.location.lat(),
      lng: location.geometry.location.lng()
    };
    callback(result);
  });
}

app.ports.initSearch.subscribe(function(data) {
  initSearch(function(result) {
    app.ports.setCoordinates.send(result);
  });
});
